import {
    DivisibleByThree,
    DivisibleByFive,
    DivisibleByFifteen}  from './divisible-functions'
import * as utils from './utils';

beforeEach(() =>{
    jest.spyOn(utils, 'ShouldTextChange').mockReturnValue(true);   
});


describe('divisiblebythree tests', () =>
{   
    it('when given number is divisible by three and for given day should return wizz', () =>{    
        utils.ShouldTextChange.mockReturnValueOnce(true);
        let response = DivisibleByThree(3);
        expect(response).toBe("wizz");
    });

    it('when given number is divisible by three and for other days should return fizz', () =>{    
        utils.ShouldTextChange.mockReturnValueOnce(false);
        let response = DivisibleByThree(3);
        expect(response).toBe("fizz");
    });

    it('when given number is not divisible by three should return given number', () =>{ 
        let response = DivisibleByThree(8);
        expect(response).toBe(8);
    })
})

describe('divisiblebyfive tests', () =>
{   
    it('when given number is divisible by five and for given day should return wuzz', () =>{    
        utils.ShouldTextChange.mockReturnValueOnce(true);
        let response = DivisibleByFive(20);
        expect(response).toBe("wuzz");
    });

    it('when given number is divisible by five and for other days should return buzz', () =>{    
        utils.ShouldTextChange.mockReturnValueOnce(false);
        let response = DivisibleByFive(20);
        expect(response).toBe("buzz");
    });

    it('when given number is not divisible by five should return given number', () =>{
        let response = DivisibleByFive(8);
        expect(response).toBe(8);
    })
})

describe('divisiblebyfifteen tests', () =>
{   
    it('when given number is divisible by fifteen and for given day should return wizz wuzz', () =>{    
        utils.ShouldTextChange.mockReturnValueOnce(true);
        let response = DivisibleByFifteen(30);
        expect(response).toBe("wizz wuzz");
    });

    it('when given number is divisible by fifteen and for other days should return fizz buzz', () =>{    
        utils.ShouldTextChange.mockReturnValueOnce(false);
        let response = DivisibleByFifteen(30);
        expect(response).toBe("fizz buzz");
    });

    it('when given number is not divisible by fifteen should return given number', () =>{
        let response = DivisibleByFifteen(8);
        expect(response).toBe(8);
    })
})