import {ShouldTextChange} from './utils';

const Divisible_By_Three_Text ="fizz";
const Divisible_By_Three_Text_For_Given_Day ="wizz";
const Divisible_By_Five_Text ="buzz";
const Divisible_By_Five_Text_For_Given_Day ="wuzz";
const Divisible_By_Fifteen_Text ="fizz buzz";
const Divisible_By_Fifteen_Text_For_Given_Day ="wizz wuzz";

const DivisibleByThree = (number) => 
number % 3 === 0 ? ShouldTextChange() ? Divisible_By_Three_Text_For_Given_Day : Divisible_By_Three_Text : number;

const DivisibleByFive = (number) =>
number % 5 === 0 ? ShouldTextChange() ? Divisible_By_Five_Text_For_Given_Day : Divisible_By_Five_Text : number;

const DivisibleByFifteen = (number) => 
number % 15 === 0 ? ShouldTextChange() ? Divisible_By_Fifteen_Text_For_Given_Day : Divisible_By_Fifteen_Text : number;

export {    
    DivisibleByThree,
    DivisibleByFive,
    DivisibleByFifteen
};