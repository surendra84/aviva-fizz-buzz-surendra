const[minValue, maxValue] = [1, 1000];
const DayForTextChange = 3;

const ErrorMessages = {
negative: 'Please enter positive number',
range: 'Number should exists between '+minValue+' and '+maxValue+''
}

const  Validate = (value) => 
value < minValue || value === "" ? ErrorMessages.negative
: (value < minValue || value > maxValue) ? ErrorMessages.range
: ""
const ShouldTextChange = () =>
new Date().getDay() === DayForTextChange;

export {     
Validate,
ShouldTextChange
};