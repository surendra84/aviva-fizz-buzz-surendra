import {ShouldTextChange, Validate} from './utils';

describe('should text change', () =>{
    it('when current day is wednesday should return true otherwise false', () =>
    {
        const response = ShouldTextChange();
        const expectedResponse = new Date().getDay() === 3;
        expect(response).toEqual(expectedResponse); 
    })
});

describe('validate', () =>{
    it('when given number is negative should return negative error message', () =>
    {
        const response = Validate(-20);
        expect(response).toEqual('Please enter positive number'); 
    });

    it('when given number is not in range should return range error message', () =>
    {
        const response = Validate(1220);
        expect(response).toEqual('Number should exists between 1 and 1000'); 
    });

    it('when given number is valid should not return error message', () =>
    {
        const response = Validate(190);
        expect(response).toEqual(''); 
    });
})

