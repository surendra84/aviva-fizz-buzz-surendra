import React from'react';
import {DivisibleByFifteen, DivisibleByThree, DivisibleByFive} from './../../utils/divisible-functions';
import {number} from 'prop-types';

const NumberFactory = (props) =>
{ 
    let components = [DivisibleByFifteen, DivisibleByFive, DivisibleByThree];
    let number = props.inputNumber;   
    for(let index = 0; index < components.length; index++)
    {             
        number = components[index](number);           
        if(number !== props.inputNumber)
        {
            break;
        }
    }   
   
    return number !== undefined ? <span className ={number}>{number}</span> : props.inputNumber;  
}

NumberFactory.defaultProps = {
    inputNumber: undefined
};

NumberFactory.propTypes = {
    inputNumber: number  
  };

export {NumberFactory};