import React from 'react';
import { shallow } from 'enzyme';
import {NumberFactory} from './number-factory';
import * as DivisibleFunctions  from './../../utils/divisible-functions';

const setUp = (props ={}) =>{
    return shallow(<NumberFactory {...props}/>);
}
beforeEach(() =>{
    jest.spyOn(DivisibleFunctions, 'DivisibleByThree').mockReturnValue(1);
    jest.spyOn(DivisibleFunctions, 'DivisibleByFive').mockReturnValue(1);
    jest.spyOn(DivisibleFunctions, 'DivisibleByFifteen').mockReturnValue(1);
});
describe('<NumberFactory/>', () =>{   
    it('When input is divisible by three retun fizz', () =>{        
        DivisibleFunctions.DivisibleByFifteen.mockReturnValueOnce(3);
        DivisibleFunctions.DivisibleByFive.mockReturnValueOnce(3);
        DivisibleFunctions.DivisibleByThree.mockReturnValueOnce('fizz');
        var response = setUp({inputNumber:3});        
        expect(response.debug()).toContain('fizz');
    });   
    it('When input is divisible by five retun buzz', () =>{
        DivisibleFunctions.DivisibleByFifteen.mockReturnValueOnce(5);
        DivisibleFunctions.DivisibleByFive.mockReturnValueOnce('buzz');
        DivisibleFunctions.DivisibleByThree.mockReturnValueOnce(5);
        var response = setUp({inputNumber:5});        
        expect(response.debug()).toContain('buzz');
    });   
    it('When input is divisible by fifteen retun fizz buzz', () =>{
        DivisibleFunctions.DivisibleByFifteen.mockReturnValueOnce('fizz buzz');
        DivisibleFunctions.DivisibleByFive.mockReturnValueOnce(45);
        DivisibleFunctions.DivisibleByThree.mockReturnValueOnce(45);
        var response = setUp({inputNumber:45});        
        expect(response.debug()).toContain('fizz buzz');
    }); 
    it('When input is not divisible by three five fifteen retun given number', () =>{
        DivisibleFunctions.DivisibleByFifteen.mockReturnValueOnce(4);
        DivisibleFunctions.DivisibleByFive.mockReturnValueOnce(4);
        DivisibleFunctions.DivisibleByThree.mockReturnValueOnce(4);
        var response = setUp({inputNumber:4});        
        expect(response.debug()).toContain(4);
    });   
})