import React from 'react';
import {string} from 'prop-types';

const Header = (props) =>{
    const{className, headerText} ={...props}
    return(<h4 className={className}>{headerText}</h4>)
}

export {Header};

Header.defaultProps ={
    className:undefined,
    headerText:undefined
}

Header.propTypes={
    className:string,
    headerText:string
}