import React from 'react';
import {Header} from './Header';
import {shallow} from 'enzyme';
import checkPropTypes from 'check-prop-types';

const setUp = (props={}) =>{
    return shallow(<Header {...props}/>);
}
describe('<Header/>', () =>{
    let expectedProps;
    beforeEach(()=>{
        expectedProps={
            className:'test',          
            headerText:'my app'
        }
    });
    it('should render without errors', () =>{
        const wrapper = setUp(expectedProps);
        expect(wrapper.length).toBe(1);
    });
    it('does not throw warning with expected props', ()=>{
        const propsError = checkPropTypes(Header.prototypes, expectedProps, 'prop', Header.name);
        expect(propsError).toBeUndefined();
    });
});