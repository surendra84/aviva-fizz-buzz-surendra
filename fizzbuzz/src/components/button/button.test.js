import React from 'react';
import {Button} from './button';
import {shallow} from 'enzyme';
import checkPropTypes from 'check-prop-types';

const setUp = (props={}) =>{
    return shallow(<Button {...props}/>);
}
describe('<Button/>', () =>{
    let expectedProps;
    beforeEach(()=>{
        expectedProps={
            className:'test',
            type:'button',
            text:'submit',
            ref:'ref',
            disabled:true,
            onClick: () =>{},
            dataTest:'submit'
        }
    });
    it('should render without errors', () =>{
        const wrapper = setUp(expectedProps);        
        expect(wrapper.length).toBe(1);
    });

    it('should call click event', () =>{
        const wrapper = setUp(expectedProps);
        const clickHandler = jest.fn();
        const event ={};
        wrapper.setProps({onClick:clickHandler});
        wrapper.simulate('click', event);
        expect(clickHandler).toHaveBeenCalled();
    });
    
    it('should return disabled prop true', () =>{        
        const wrapper = setUp(expectedProps);
        expect(wrapper.prop('disabled')).toBeTruthy();
    });  

    it('does not throw warning with expected props', ()=>{
        const propsError = checkPropTypes(Button.prototypes, expectedProps, 'prop', Button.name);
        expect(propsError).toBeUndefined();
    });
});