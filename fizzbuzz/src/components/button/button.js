import React from 'react';
import {string, bool, func} from 'prop-types';

const Button = (props) =>
{
    const {
            className, 
            type, 
            text, 
            onClick, 
            disabled, 
            dataTest,
            arialabel
          } = {...props};
    return (
        <button 
              className = {className} 
              type={type} 
              disabled={disabled} 
              onClick={onClick} 
              data-test={dataTest}
              aria-label={arialabel}>
              {text}
        </button>
      );
};

Button.defaultProps ={
    className: undefined,
    disabled: false,
    type:"button",
    text:undefined,
    onClick:() => undefined,
    dataTest:undefined,
    arialabel:undefined
}

Button.propTypes ={
    className: string,
    disabled: bool,
    type: string,
    onClick: func,
    dataTest:string,
    arialabel:string
}

export { Button };