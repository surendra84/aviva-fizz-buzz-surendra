import React from 'react';
import {Label} from './label';
import {shallow} from 'enzyme';
import checkPropTypes from 'check-prop-types';

const setUp = (props={}) =>{
    return shallow(<Label {...props}/>);
}
describe('<Label/>', () =>{
    let expectedProps;
    beforeEach(()=>{
        expectedProps={
            className:'test',          
            text:'number',
            type:'label',
            dataTest:'number'
        }
    });
    it('should render without errors', () =>{
        const wrapper = setUp(expectedProps);
        expect(wrapper.length).toBe(1);
    });
    it('does not throw warning with expected props', ()=>{
        const propsError = checkPropTypes(Label.prototypes, expectedProps, 'prop', Label.name);
        expect(propsError).toBeUndefined();
    });
});