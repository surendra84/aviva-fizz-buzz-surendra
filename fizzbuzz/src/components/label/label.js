import React from 'react';
import {string} from 'prop-types';

const Label= (props) =>{
    const{type, className, text, dataTest} = {...props};
    return(
    <label type={type} className={className} data-test={dataTest}>{text}</label>);
}

Label.defaultProps ={
    className: undefined,  
    type:"label",
    text:undefined,
    dataTest: undefined   
};

Label.propTypes ={
    className: string,
    type: string,
    text: string,
    dataTest:string
}

export {Label};