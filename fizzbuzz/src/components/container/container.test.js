import React from 'react';
import {Container} from './container';
import {mount} from 'enzyme';
import {findByTestAttribute} from './../../testutils';

const setUp =() =>{
return mount(<Container/>);
};

describe("<Container/>", () =>{
    it('render without crashing', () =>{
        const wrapper = setUp();
        expect(wrapper).toBeTruthy();
    });
    it('render submit button', () =>{        
        const wrapper = setUp();
        const submitButton = findByTestAttribute(wrapper,'submit-button');
        expect(submitButton.length).toBe(1);
    });  
    it('render textbox', () =>{
        const wrapper = setUp();
        const inputField = findByTestAttribute(wrapper,'input-number');
        expect(inputField.length).toBe(1);
    });
    
    it('render label', () =>{
        const wrapper = setUp();
        const labelField = findByTestAttribute(wrapper,'label-field');
        expect(labelField.length).toBe(1);
    });
})