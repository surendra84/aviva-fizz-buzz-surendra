import React, {useState} from 'react';
import {Label} from './../label/label';
import{Textbox} from './../textbox/textbox';
import {Button} from './../button/button';
import {NumberFactory} from './../numberfactory/number-factory';
import {Validate} from './../../utils/utils';
import {Pagination} from './../pagination/pagination';

const  Container = () => {
  const textboxRef = React.createRef();
  const[numbers, renderNumbers] = useState([]);
  const[errorMessage, currentErrorMessage]= useState();
  const[currentPage, setCurrentPage] = useState(1);
  const numbersPerPage = "20";
  
  const indexOfLastNumber = currentPage * numbersPerPage;
  const indexOfFirstNumber = indexOfLastNumber- numbersPerPage;
  const currentNumbers = numbers.slice(indexOfFirstNumber, indexOfLastNumber); 

  let validationMessage = "";
  const handleClick = (event) =>
  { 
    let givenNumber = parseInt(textboxRef.current.value);
    validationMessage = Validate(givenNumber);    
    if(validationMessage === "")
    {
      let displayNumbers =[];
      for (let index = 1; index <= givenNumber; index++) {
         displayNumbers.push(index);          
      }
      renderNumbers(displayNumbers);
      setCurrentPage(1);
    }
    else
    {
      currentErrorMessage(validationMessage);
      renderNumbers([]);
    }
  }

  const paginate = (directon) => { 
    let pageNumber = currentPage; 
    setCurrentPage(directon === 'prev' ? pageNumber -1: pageNumber +1);
  }

  return (
    <>
        <Label text="Enter number: " type="label" dataTest="label-field"/>     
        <Textbox className = "textbox"  type="text" dataTest="input-number" ariaLabel ="input number" ref ={textboxRef} />
        <Button className = "buttonField"  type="button" text ="Submit" dataTest="submit-button" ariaLabel="submit" onClick={handleClick}></Button>
        { numbers.length > 0 ? 
            <>                                
            <ul className = "list-group-mb4" data-test="table">
            {currentNumbers.map(number => (                                  
                <li className ="list-group-item" key ={number}><NumberFactory inputNumber ={number}/></li>
            ))}
            </ul>  
            <Pagination numbersPerPage ={numbersPerPage} totalNumbers ={numbers.length} currentPage ={currentPage} paginate={paginate} data-test="pages"></Pagination> 
            </>
            :  errorMessage !== "" ? <span className="error">{errorMessage}</span> : null  
          }           
    </>
  );
}

export {Container};
