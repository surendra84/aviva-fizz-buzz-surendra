import React from 'react';
import { mount } from 'enzyme';
import {Pagination} from './pagination';
import {findByTestAttribute} from './../../testutils';

const setUp = (props = {}) =>{
    return mount(<Pagination {...props}/>);
}



describe('<Pagination/>', () =>{
    let wrapper;
    let expectedProps;
    beforeEach(() => {
        expectedProps =  {
        paginate: ()=>{},
        totalNumbers: 40,
        numbersPerPage: 20,
        currentPage:1
        };
        wrapper = setUp(expectedProps);
    });

    it('should render without any error', () =>{      
        expect(wrapper).toBeTruthy();
    });

    it('should render pagination previous button and disbled true', () =>{   
        const prevButton = findByTestAttribute(wrapper,'page-prev-button');   
        expect(prevButton.length).toBe(1);
        expect(prevButton.prop('disabled')).toBeTruthy();
    });

    it('should render pagination next button and disbled false', () =>{   
        const nextButton = findByTestAttribute(wrapper,'page-next-button');   
        expect(nextButton.length).toBe(1);
        expect(nextButton.prop('disabled')).toBeFalsy();
    });
})