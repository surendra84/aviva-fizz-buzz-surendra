import React from 'react';
import {Button} from '../button/button';
import {func, number} from 'prop-types';

const Pagination = (props)=>{
    const{numbersPerPage, totalNumbers, paginate, currentPage} = {...props}
    const pagesCount = Math.ceil(totalNumbers/numbersPerPage);    
    
    return(
        <nav>
            <ul className ="pagination" data-test="page-numbers">            
                <li className="page-item" data-test="page">
                   <Button 
                        data-test="page-prev-button" 
                        className = "btn btn-info" 
                        text ="<prev" 
                        disabled={currentPage === 1} 
                        onClick = {() =>paginate('prev')}>
                    </Button> 
                </li> 
                <li className="page-item" data-test="page">
                   <Button 
                        data-test="page-next-button" 
                        className = "btn btn-info" 
                        text ="next>" 
                        disabled={currentPage === pagesCount} 
                        onClick = {() =>paginate('next')}>
                    </Button> 
                </li>          
            </ul>
        </nav>
    );
}

Pagination.defaultProps ={
    numbersPerPage: undefined,
    currentPage:undefined,
    totalNumbers:undefined,
    paginate: () => undefined,
}

Pagination.prototypes ={
    numbersPerPage:number,
    currentPage: number,
    totalNumbers:number,
    paginate:func
}
export{Pagination};