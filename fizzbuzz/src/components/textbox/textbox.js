import React from 'react';
import {string, number, func} from 'prop-types';

const Textbox = React.forwardRef((props, forwardref) => {   
    const{className, type, dataTest, ariaLabel} ={...props};
 return  <input 
                type={type} 
                className = {className} 
                ref={forwardref} 
                data-test={dataTest}
                aria-label={ariaLabel}>               
        </input>
})

Textbox.defaultProps = {
    className: undefined,
    type:"text",
    ariaLabel: undefined,
    dataTest:undefined,
    onBlur:() => undefined,
    onChange:()=> undefined
}

Textbox.propTypes = {
    className: string,
    fieldProps: number,
    onBlur: func,
    onChange: func,
    ariaLabel:string,
    dataTest:string
  };

export {Textbox};