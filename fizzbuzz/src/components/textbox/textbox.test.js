import React from 'react';
import {Textbox} from './textbox';
import {shallow} from 'enzyme';
import checkPropTypes from 'check-prop-types';

const setUp = (props={}) =>{
    return shallow(<Textbox {...props}/>);
}
describe('<Textbox/>', () =>{
    let expectedProps;
    beforeEach(()=>{
        expectedProps={
            className:'test',
            type:'text',
            onChange: () =>{},
            onBlur:() =>{},
            dataTest:'input number'
        }
    });
    it('should render without errors', () =>{
        const wrapper = setUp(expectedProps);
        expect(wrapper.length).toBe(1);
    });
    it('does not throw warning with expected props', ()=>{
        const propsError = checkPropTypes(Textbox.prototypes, expectedProps, 'prop', Textbox.name);
        expect(propsError).toBeUndefined();
    });
});