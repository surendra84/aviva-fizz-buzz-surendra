import React from 'react';
import {Header} from './components/header/header';
import {Container} from './components/container/container';

export const App = () => {
  return (
    <>
        <Header className="display-4" headerText="Fizz Buzz App" data-test="header"/>        
        <Container data-test="container"/>
    </>
  );
}

export default App;
