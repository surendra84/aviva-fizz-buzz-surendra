import React from 'react';
import {App} from './App';
import {mount} from 'enzyme';
import {findByTestAttribute} from './../src/testutils';

const setUp =() =>{
return mount(<App/>);
};

describe('<App/>', () =>{
  it('renders with out crashing', ()=>
  {
   const wrapper = setUp();
   expect(wrapper.length).toBe(1);
  });

  it('renders header component', ()=>
  {
   const wrapper = setUp();
   const header= findByTestAttribute(wrapper, "header");
   expect(header.length).toBe(1);
  });

  it('renders container component', ()=>
  {
   const wrapper = setUp();
   const container= findByTestAttribute(wrapper, "container");
   expect(container.length).toBe(1);
  });
})

